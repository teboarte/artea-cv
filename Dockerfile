FROM node:lts AS runtime
WORKDIR /app
RUN npm i -g pnpm@latest
RUN pnpm i
RUN pnpm run build

ENV HOST=0.0.0.0
ENV PORT=4321
EXPOSE 4321
CMD pnpm start
